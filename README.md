# Keyboard Config

This repository is split into two folders `general` and `keyboard-specific`. [general](#general) covers configuration agnostic to any specific keyboard, and is generally operating system wide. [Keyboard Specific](#keyboard-specific) focuses on individual keyboards and will have things like QMK files.

## General

### Windows

#### Changes

| Shortcut | Map |
| -------- | --- |
| PrtSc    | Windows Shift s |

#### Requirements

- [AutoHotKey](https://www.autohotkey.com/)

#### Usage

Double click `shortcuts.ahk`, autohotkey will load it.

To autostart during startup run the following:

1. Open start, search for `run`
2. Enter `shell:startup`
3. Copy and paste `shortcuts.ahk` into the folder.

## Keyboard Specific

### IBM Model F XT

### Changes

Does the following mappings:

| Shortcut | Map |
| ------------ | --- |
| Shift PrtSc  | PrtSc |
| Shift F1-10  | F11-20 |
| Ctrl Backspace  | Delete |
| Ctrl F1-4  | F21-24 |
| Ctrl F5  | Previous Page |
| Ctrl F6  | Next Page |
| Ctrl F7  | Play/Pause |
| Ctrl F8  | Mute |
| Ctrl F9  | Volume Down |
| Ctrl F10  | Volume Up |


#### Requirements

- Soarer Converter (I use [tinkerBOY](https://www.tinkerboy.xyz/product/tinkerboy-xt-at-usb-converter-with-soarers-converter-firmware/))
- [Soarer Converter Software](https://deskthority.net/viewtopic.php?t=2510)

#### Usage

Within the Soarer converter software folder there will be a couple of binaries in the `tools` folder depending on your operating system. The relevant ones to apply `macros.sc` are `scas` and `scwr`. Run the following while the converter is plugged in:

`scas macros.sc macros.scb`

`scwr macros.scb`

If you want to undo these macros the soarers software includes an empty sc file which can be used inplace.
